package com.pouwelse.eclipse.preferences.loader.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IExportedPreferences;
import org.eclipse.core.runtime.preferences.IPreferenceNodeVisitor;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IStartup;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

public class PreferencesLoaderService implements IStartup {

	@Override
	public void earlyStartup() {
		importPreferences();
		
		ResourcesPlugin.getWorkspace().addResourceChangeListener(new IResourceChangeListener() {

			@Override
			public void resourceChanged(IResourceChangeEvent event) {
				importPreferences();

			}
		});
	}

	private void importPreferences() {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					importUserPreferences();
					importWorkspacePreferences();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}

			}
		});
	}
	
	private void importUserPreferences() {
		File userHome = new File(System.getProperty("user.home"));
		File preferencesFile = new File(userHome, "eclipse-preferences.epf");
		
		if (preferencesFile.exists()) {
			try{ 
				doImport(preferencesFile);
			}catch(CoreException | FileNotFoundException | BackingStoreException e){
				System.out.println("Failed to import " + e.getMessage());
			}
		}

	}
	private void importWorkspacePreferences() {
		Location instanceLocation = Platform.getInstanceLocation();
		File workspaceFile = new File(instanceLocation.getURL().getPath());
		File preferencesFile = new File(workspaceFile, "eclipse-preferences.epf");
		
		
		if (preferencesFile.exists()) {
			try{ 
				doImport(preferencesFile);
			}catch(CoreException | FileNotFoundException | BackingStoreException e){
				System.out.println("Failed to import " + e.getMessage());
			}
		} 

	}
	
	private void doImport(File preferencesFile) throws BackingStoreException, FileNotFoundException, CoreException{
		IPreferencesService preferencesService = Platform.getPreferencesService();
		IExportedPreferences preferences = preferencesService.readPreferences(new FileInputStream(preferencesFile));
		final IEclipsePreferences rootNode = preferencesService.getRootNode();
		preferences.accept(new IPreferenceNodeVisitor(){

			@Override
			public boolean visit(IEclipsePreferences node) throws BackingStoreException {
				String[] childrenNames = node.keys();
				Preferences n = rootNode.node(node.absolutePath());
				for (String c : childrenNames){
					n.put(c, node.get(c, null));
				}
				
				return true;
			}}
		);
		
		rootNode.flush();
		
	}

}
